# Create service, deployment and namespace
kubectl apply -f demo/namespace.yaml
kubectl apply -f demo/deployment.yaml
kubectl apply -f demo/service.yaml

# Check pods and services
kubectl get pods -n space1 -o wide
kubectl get services -n space1 -o wide

# Verify route propagation
vagrant ssh leaf1 -c "sudo net show bgp "
vagrant ssh leaf2 -c "sudo net show bgp "

# Scale deployment
kubectl scale deployment/nginx-deployment --replicas=6 -n space1

# Check pods
kubectl get pods -n space1 -o wide

# Verify route propagation
vagrant ssh leaf1 -c "sudo net show bgp "
vagrant ssh leaf2 -c "sudo net show bgp "
